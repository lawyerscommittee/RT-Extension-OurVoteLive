# TODO: Re-add stalled state
Set( %Lifecycles, election_protection => {
    initial        => [ 'new' ],
    active         => [ 'open', 'parked' ],
    inactive       => [ 'resolved', 'expired', 'rejected', 'deleted' ],

    defaults       => {
	on_create  => 'new',
    },

    transitions => {
                      # 'new', 'parked', 'open', 'resolved', 'expired', 'rejected', 'deleted' #
    ''             => [ 'new', 'parked', 'open', 'resolved'                                   ],
    new            => [        'parked', 'open',             'expired', 'rejected', 'deleted' ],
    open           => [        'parked',                     'expired', 'rejected', 'deleted' ],
    parked         => [                  'open',             'expired', 'rejected', 'deleted' ],
    resolved       => [                                                             'deleted' ],
    expired        => [                                                             'deleted' ],
    rejected       => [                                                             'deleted' ],
    deleted        => [ 'new', 'parked', 'open', 'resolved', 'expired', 'rejected'            ],
    },

    rights => {
	'* -> *'       => 'ModifyTicket',
	'* -> deleted' => 'DeleteTicket',
	'deleted -> *' => 'DeleteTicket',
    },

    actions        => [
	'* -> open' => {
	    label => 'Open',
	    update => 'Comment',
	},
	'* -> expired' => {
	    label => 'Expire',
	},
	'* -> parked' => {
	    label => 'Park',
	},
	'* -> resolved' => {
	    label => 'Resolved',
	    update => 'Comment',
	},
	'* -> rejected' => {
	    label => 'Reject',
	    update => 'Comment',
	},
	'* -> deleted' => {
	    label => 'Deleted',
	},
	],
  },
);

1;
