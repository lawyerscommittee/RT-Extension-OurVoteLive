try:
    from http.cookiejar import CookieJar
    from urllib.request import HTTPCookieProcessor, HTTPSHandler, build_opener
except ImportError:
    from cookielib import CookieJar
    from urllib2 import HTTPCookieProcessor, HTTPSHandler, build_opener

import os, pickle, ssl, sys
from flask import Flask, redirect, request, send_from_directory, session, url_for
app = Flask(__name__)

ovl_server = None
ovl_user = None
ovl_pass = None

# Serve static files
@app.route('/')
def index():
    if login_needed():
        return redirect(url_for('login'))
    return app.send_static_file('index.html')

@app.route('/login')
def login():
    return app.send_static_file('login.html')

@app.route('/do-login', methods=['POST'])
def do_login():
    global ovl_server, ovl_user, ovl_pass
    ovl_server = 'https://' + request.form['server'] + '.ourvotelive.org'
    ovl_user = request.form['user']
    ovl_pass = request.form['password']
    if not login_to_ovl():
        return 'Login to OVL failed. Click Back button to try again'
    return redirect(url_for('index'))

@app.route('/NoAuth/Logout.html')
def logout():
    print('Logging out')
    response = logout_of_ovl()
    print('Response Code', response.code)
    return 'Logged Out'

@app.route('/app/<path:path>')
def send_app(path):
    return send_from_directory('static/app', path)

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)

@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('static/img', path)

@app.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory('static/fonts', path)

# Wrap OVL REST calls
@app.route('/REST/<path:path>', methods=['GET', 'POST'])
def ajax_wrapper(path):
    if request.query_string:
        path += '?' + request.query_string
    print('Making request', path)
    response = make_ovl_request(ovl_server + '/REST/' + path, request.get_data())
    print('Response Code', response.code)
    return response.read()

def login_needed():
    return 'ovl_user' not in session or \
            session['ovl_user'] != ovl_user or \
            not logged_in_to_ovl()

def logged_in_to_ovl():
    response = make_ovl_request(ovl_server, None)
    return 'RT.CurrentUser = {};' not in response.read()

def login_to_ovl():
    if 'ovl_user' in session: del session['ovl_user']
    if 'ovl_cookies' in session: del session['ovl_cookies']
    response = make_ovl_request(ovl_server + '/NoAuth/Login.html', 'user='+ovl_user+'&pass='+ovl_pass)
    if 'Your username or password is incorrect' in str(response.read()):
        return False
    print('Logged into ' + ovl_server)
    session['ovl_user'] = ovl_user
    return True

def logout_of_ovl():
    if 'ovl_user' in session: del session['ovl_user']
    if 'ovl_cookies' in session: del session['ovl_cookies']
    return make_ovl_request(ovl_server + '/NoAuth/Logout.html', None)

def make_ovl_request(url, data):
    # build our opener
    # use a cookie jar to maintain OVL session cookies
    # use an HTTPS handler with unverified context because OVL dev instances don't have proper certs
    cookieJar = CookieJar()
    cookie_processor = HTTPCookieProcessor(cookieJar)
    https_handler = HTTPSHandler(context=ssl._create_unverified_context())
    opener = build_opener(cookie_processor, https_handler)
    opener.addheaders = [('Referer', ovl_server)]

    # load OVL session cookies
    if 'ovl_cookies' in session:
        for cookie in pickle.loads(session['ovl_cookies']):
            cookieJar.set_cookie(cookie)
    
    # make the request
    if data is not None:
        data = data.encode('ascii')
    response = opener.open(url, data)

    # store the latest OVL cookies
    session['ovl_cookies'] = pickle.dumps(list(cookieJar))

    return response

if __name__ == "__main__":
    app.secret_key = 'dev-secret-key'
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
