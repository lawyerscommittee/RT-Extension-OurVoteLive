<%init>
my $Config = {};
$Config->{$_} = RT->Config->Get( $_, $session{CurrentUser} )
  for qw(rtname WebPath MessageBoxRichTextHeight);

my $CurrentUser = {};
if ($session{CurrentUser} and $session{CurrentUser}->id) {
    $CurrentUser->{$_} = $session{CurrentUser}->$_
      for qw(id Name EmailAddress RealName);

    $CurrentUser->{Privileged} = $session{CurrentUser}->Privileged
        ? JSON::true : JSON::false;

    $Config->{WebHomePath} = RT->Config->Get("WebPath")
        . (!$session{CurrentUser}->Privileged ? "/SelfService" : "");
}

my $Catalog = {
    quote_in_filename => "Filenames with double quotes can not be uploaded.", #loc
};
$_ = loc($_) for values %$Catalog;

$m->callback(
    CallbackName    => "Data",
    CurrentUser     => $CurrentUser,
    Config          => $Config,
    Catalog         => $Catalog,
);
</%init>
window.RT = {};
RT.CurrentUser = <% JSON( $CurrentUser ) |n%>;
RT.Config      = <% JSON( $Config      ) |n%>;

RT.I18N = {};
RT.I18N.Catalog = <% JSON( $Catalog ) |n %>;

% $m->abort();
