window.OVL = window.OVL || {};
window.OVL.CFconfig = window.OVL.CFconfig || {};

window.OVL.CFconfig['<% $QueueName %>'] = (function () {

    var CF = JSON.parse(decodeURIComponent("<% $CFJSONStr | nu %>"));
    var idMap = JSON.parse(decodeURIComponent("<% $idMapJSONStr | nu %>"));
    var basedOnCFs = JSON.parse(decodeURIComponent("<% $BasedOnCFsJSONStr | nu %>"));
    var basedOnMap = JSON.parse(decodeURIComponent("<% $BasedOnMapJSONStr | nu %>"));
    
    return {
        CF: CF,
	idMap: idMap,
	basedOnCFs: basedOnCFs,
	basedOnMap: basedOnMap,

	getById: function (id) {
	    var CFName = idMap[id];
	    return this.CF[CFName];
	},

	isBasedOn: function(CFName) {
	    if ($.inArray(this.CF[CFName].id, basedOnCFs) === -1) {
		return false;
	    } else {
		return true;
	    }
	},

	// Only goes one BasedOn-deep. But should be enough
	// for Handlebars recursive partials?
	getRichCF: function (CFName) {
	    // Clone the CF to be 'enriched' with BasedOn Values
	    var richCF = $.extend(true, {}, this.CF[CFName]);
	    var CFid = richCF.id;
	    var basedOnCFNames = basedOnMap[CFid];

	    // For each CF that bases on richCF...
	    for (i=0, j=basedOnCFNames.length; i<j; i++) {
		CFName = basedOnCFNames[i];
		var basedOnValues = this.CF[CFName].Values;

		// For each Value of this CF that bases on richCF...
		for (x=0, y=basedOnValues.length; x<y; x++) {
		    var Value = basedOnValues[x];

		    // Find each Value of richCF that this Value of
		    // the basing CF lists as its Category...
		    for (a=0, b=richCF.Values.length; a<b; a++) {
			var Category = richCF.Values[a];

			if (Value.Category === Category.Name) {

			    if (richCF.Values[a].BasedOnValues === undefined) {
				richCF.Values[a].BasedOnValues = [];
			    }

			    // ...and add this basing CF's Value to the
			    // richCF's Value.
			    richCF.Values[a].BasedOnValues.push({
				CF: this.CF[CFName], Value: Value
			    });
			}
		    }
		}
	    }

	    return richCF;
	}
    };
}());

% $m->abort();

<%init>

use JSON;
use Compress::Zlib;
use MIME::Base64;
use Data::Dumper;

my $Queue = RT::Queue->new( $session{'CurrentUser'} );

$Queue->Load($QueueName);

my %CFHash = ();
my %idMap = ();
my @BasedOnCFs = ();

# Custom Field 'dependency' mapping.
my %BasedOnMap = ();

if ($session{'CurrentUser'}->HasRight( Object => $Queue, Right => "SeeQueue" )) {

    if ($session{'CurrentUser'}->HasRight( Object => $Queue, Right => "SeeCustomField" ) ) {
	# Get all Custom Fields for *this Queue*. Includes Global CFs.
	my $CustomFields = $Queue->TicketCustomFields();

	while ( my $CF = $CustomFields->Next() ) {

	    $idMap{ $CF->id } = $CF->Name;

	    # Create an array from all predefined Values for this
	    # Custom Field.
	    my $valueGen = $CF->Values;
	    my @Values = ();
	    while (my $value = $valueGen->Next) {
		push(@Values, { Name => $value->Name,
				id => $value->id,
				Description => $value->Description,
				SortOrder => $value->SortOrder,
				Category => $value->Category,
			     });
	    }

	    # Create a hash from all the standard properties of this
	    # Custom Field.
	    my %CFconfig = (
		Name => $CF->Name,
		id => $CF->id,
		IsSelectionType => $CF->IsSelectionType,
		Type => $CF->Type,
		TypeComposite => $CF->TypeComposite,
		FriendlyType => $CF->FriendlyType,
		SingleValue => $CF->SingleValue,
		MaxValues => $CF->MaxValues,
		Pattern => $CF->Pattern,
		FriendlyPattern => $CF->FriendlyPattern,
		Description => $CF->Description,
		SortOrder => $CF->SortOrder,
		Disabled => $CF->Disabled,
		Values => \@Values,
		);

	    # Some Custom Fields, like Select-types, have Render Types.
            # If so, add this Custom Field's Render Types to its hash.
	    if ($CF->HasRenderTypes) {
		$CFconfig{'RenderType'} = $CF->RenderType;
		$CFconfig{'DefaultRenderType'} = $CF->DefaultRenderType;
		$CFconfig{'RenderTypes'} = $CF->RenderTypes;
	    }

	    if ($CF->BasedOn) {
		$CFconfig{'BasedOn'} = $CF->BasedOn;

		push (@BasedOnCFs, $CF->BasedOn);

		if (! exists $BasedOnMap{$CF->BasedOn}) {
		    $BasedOnMap{$CF->BasedOn} = [];
		}
		push (@{$BasedOnMap{$CF->BasedOn}},
		      $CF->Name);
	    }

            # Add this Custom Field's parameter hash to
	    # the Queue's CFconfig hash.
	    $CFHash{$CF->Name} = \%CFconfig;

	}

    }
}

my $CFJSONStr = encode_json \%CFHash;
my $idMapJSONStr = encode_json \%idMap;
my $BasedOnCFsJSONStr = encode_json \@BasedOnCFs;
my $BasedOnMapJSONStr = encode_json \%BasedOnMap;

#my $d = deflateInit() or die "Cannot create a deflation stream\n" ;
#my ($zippedCFs, $zflush, $status);
#my $zippedCFs64;

#($zippedCFs, $status) = $d->deflate($CFJSONStr);
#$status == Z_OK or die "deflation failed\n" ;
#($zflush, $status) = $d->flush();
#$status == Z_OK or die "deflation failed\n" ;
#$zippedCFs .= $zflush;
#$zippedCFs64 = encode_base64($zippedCFs);

</%init>
<%args>
$QueueName => "Election Protection"
</%args>
