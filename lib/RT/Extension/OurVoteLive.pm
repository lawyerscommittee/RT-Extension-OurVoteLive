package RT::Extension::OurVoteLive;
use strict;
use warnings;

use Data::Dumper;

use Exporter qw(import);

our $VERSION = '0.02';

our @EXPORT_OK = qw(UpdateAdminCcRegionGroups);

=head1 NAME

RT-Extension-OurVoteLive - The customizations that turn a RT into a RT for rt.ourvotelive.org

=head1 RT VERSION

Works with RT 4.2 and 4.4.

=head1 AUTHOR

Lawyers' Committee for Civil Rights Under LawE<lt>bduggan@lawyerscommittee.orgE<gt>

=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2016 by Laywers' Committee for Civil Rights Under Law

This is free software, licensed under:

  The GNU General Public License, Version 2, June 1991

=cut

# Get "Group states" CF
our $groupStatesCf = RT::CustomField->new(RT->SystemUser);
$groupStatesCf->LoadByName(Name=>"Group states");

# Get "Group counties" CF
our $groupCountiesCf = RT::CustomField->new(RT->SystemUser);
$groupCountiesCf->LoadByName(Name=>"Group counties");

sub _DeleteWatchers {
    my $adminCcs = shift;
    my $ticketObj = shift;
    
    while (my $adminCcMember = $adminCcs->Next) {
	$ticketObj->DeleteWatcher(
	        PrincipalId => $adminCcMember->id,
	        Type => 'AdminCc',
	    );
    }
}

sub _LimitToStateMatchGroups {
    my $groups = shift;
    my $state = shift;

    # Limit Members to those with a list of States that includes the Ticket's State
    $groups->LimitCustomField(
	CUSTOMFIELD => $groupStatesCf->id,
	OPERATOR => '=',
	VALUE => $state,
	);
}

sub _LimitToStateMatchNonNullCountyGroups {
    my $groups = shift;
    my $state = shift;

    _LimitToStateMatchGroups(
	$groups, $groupStatesCf->id, $state
	);
        
    # Limit Members to those with a list of counties. No Group
    # county value matches any Ticket County value. Groups with a
    # list of counties don't match Tickets with a County value
    $groups->LimitCustomField(
	CUSTOMFIELD => $groupCountiesCf->id,
	OPERATOR => 'IS NOT',
	VALUE => undef,
	);
}

sub _DeleteStateMismatchWatchers {
    my $adminCcs = shift;
    my $state = shift;
    my $ticketObj = shift;
    
    # Limit Members to those with a list of States that does not includes the Ticket's State
    $adminCcs->LimitCustomField(
	CUSTOMFIELD => $groupStatesCf->id,
	OPERATOR => '!=',
	VALUE => $state,
	);

    _DeleteWatchers ($adminCcs, $ticketObj);
}

sub _DeleteStateMatchNullCountyWatchers {
    my $adminCcs = shift;
    my $state = shift;
    my $ticketObj = shift;
    
    _LimitToStateMatchNonNullCountyGroups($adminCcs, $state);
    
    _DeleteWatchers ($adminCcs, $ticketObj);
}

sub _DeleteStateMatchCountyMismatchWatchers {
    my $adminCcs = shift;
    my $state = shift;
    my $county = shift;
    my $ticketObj = shift;
    
    _LimitToStateMatchNonNullCountyGroups($adminCcs, $state);
    
    # If the Member's list of counties does not include the Ticket's County,
    # Remove them.
    while (my $adminCc = $adminCcs->Next) {
	my $groupHasCounty = $adminCc->
	        CustomFieldValues($groupCountiesCf->id)->
		HasEntry($county);
	
	if (!$groupHasCounty) {
	        $ticketObj->DeleteWatcher(
		    PrincipalId => $adminCc->id,
		    Type => 'AdminCc',
		    );
	}
    }
}

sub _AddStateMatchNullCountyWatchers {
    my $regionsGroups = shift;
    my $state = shift;
    my $ticketObj = shift;

    _LimitToStateMatchGroups($regionsGroups, $state);
        
    # Limit Members to those without any value for Group counties
    $regionsGroups->LimitCustomField(
	CUSTOMFIELD => $groupCountiesCf->id,
	OPERATOR => 'IS',
	VALUE => undef,
	);

    # Add them.
    while (my $regionGroup = $regionsGroups->Next) {
	$ticketObj->AddWatcher(
	        PrincipalId => $regionGroup->id,
	        Type => 'AdminCc',
	    );
    }
}

sub _AddStateMatchCountyMatchWatchers {
    my $regionsGroups = shift;
    my $state = shift;
    my $county = shift;
    my $ticketObj = shift;

    _LimitToStateMatchGroups($regionsGroups, $state);
    
    # Limit Members to those with any value for Group counties
    $regionsGroups->LimitCustomField(
	CUSTOMFIELD => $groupCountiesCf->id,
	OPERATOR => 'IS NOT',
	VALUE => undef,
	);

    while (my $regionGroup = $regionsGroups->Next) {
	my $groupHasCounty = $regionGroup->
	        CustomFieldValues($groupCountiesCf->id)->
		HasEntry($county);

	# If a Members' list of Counties includes the Ticket's
	# County, Add it
	if ($groupHasCounty) {
	        $ticketObj->AddWatcher(
		    PrincipalId => $regionGroup->id,
		    Type => 'AdminCc',
		    );
	}
    }
}

sub _DeleteAdminCcRegionGroups {
    my $ticketObj = shift;

    # Get the current State and County of the ticket
    my $state = $ticketObj->FirstCustomFieldValue('State');
    my $county = $ticketObj->FirstCustomFieldValue('County');

    my $adminCcs = $ticketObj->AdminCc->GroupMembersObj;
    $adminCcs->LimitToUserDefinedGroups;

    # We only want to delete Groups with a value for 'Group states'
    $adminCcs->LimitCustomField(
	CUSTOMFIELD => $groupStatesCf->id,
	OPERATOR => 'IS NOT',
	VALUE => undef,
	);

    for my $adminCc (@{$adminCcs->ItemsArrayRef}) {
	print $adminCc->Name ."\n";
    }
    
    if (!$state) {
	_DeleteWatchers ($adminCcs, $ticketObj);
    }
    else {
	_DeleteStateMismatchWatchers(
	        $adminCcs, $state, $ticketObj,
	    );

	_DeleteStateMatchNullCountyWatchers(
	        $adminCcs, $state, $groupCountiesCf->id, $ticketObj,
	    );

	if ($county) {
	        _DeleteStateMatchCountyMismatchWatchers(
		    $adminCcs, $state, $county, $ticketObj,
		    );
	}
    }
}

sub _AddAdminCcRegionGroups {
    my $ticketObj = shift;
    
    # Get the current State and County of the ticket
    our $state = $ticketObj->FirstCustomFieldValue('State');
    our $county = $ticketObj->FirstCustomFieldValue('County');

    my $regionsGroup = RT::Group->new(RT->SystemUser);
    $regionsGroup->LoadUserDefinedGroup('EP Regions');

    my $regionsGroups = $regionsGroup->GroupMembersObj;
    $regionsGroups->LimitToUserDefinedGroups;

    if ($state) {
	_AddStateMatchNullCountyWatchers(
	        $regionsGroups, $state, $ticketObj,
	    );

	if ($county) {
	        _AddStateMatchCountyMatchWatchers(
		    $regionsGroups, $state, $county, $ticketObj,
		    );
	}
    }

}

sub DeleteAdminCcRegionGroups {
    my $ticketObj = $_[1];
    _DeleteAdminCcRegionGroups($ticketObj);
}

sub AddAdminCcRegionGroups {
    my $ticketObj = $_[1];
    _AddAdminCcRegionGroups($ticketObj);
}

sub UpdateAdminCcRegionGroups {
    my $ticketObj = $_[1];

    _DeleteAdminCcRegionGroups($ticketObj);
    _AddAdminCcRegionGroups($ticketObj);
}

1;
