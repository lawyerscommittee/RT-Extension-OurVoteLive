package RT::Condition::OVL_RegionChange;
use base 'RT::Condition';
use strict;
use warnings;

=head2 DESCRIPTION

=cut

sub IsApplicable {
    my $DEBUG = 1;
    my $ScripName = 'RegionChange Condition';

    my $self = shift;
    my $ticket = $self->TicketObj;
    my $transaction = $self->TransactionObj;

    my $DebugHeader = $ScripName.':Ticket '.$self->TicketObj->id;
    
    my $cf = RT::CustomField->new(RT::SystemUser());
    $cf->Load($transaction->Field);
    
    if ($cf->Name eq 'State' or $cf->Name eq 'County') {
        RT::Logger->warning($DebugHeader.':Transaction Field:'.$cf->Name) if $DEBUG;
        RT::Logger->warning($DebugHeader.':Applicable') if $DEBUG;
        return 1;
    }
    RT::Logger->warning($DebugHeader.':Not Applicable') if $DEBUG;
    return 0;
}

1;
