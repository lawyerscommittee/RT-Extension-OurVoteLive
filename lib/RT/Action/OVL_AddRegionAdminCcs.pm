package RT::Action::OVL_AddRegionAdminCcs;

use strict;
use warnings;
use base qw(RT::Action);

=head1 NAME

RT::Action::AddRegionAdminCcs - OVL's script to set regional access controls on Ticket creation

=head1 DESCRIPTION

=cut

sub Prepare {
    my $self = shift;
    if ($self->TicketObj->FirstCustomFieldValue('State')) {
        return 1;
    }
    return 0;
}

sub Commit {
    my $self = shift;
    RT::Extension::OurVoteLive->AddAdminCcRegionGroups($self->TicketObj);
    return 1;
}

1;
