package RT::Action::OVL_AddProgramAdminCcs;

use strict;
use warnings;
use base qw(RT::Action);

=head1 NAME

RT::Action::AddProgramAdminCcs - OVL's script to set regional access controls

=head1 DESCRIPTION

=cut

sub Prepare {
    return 1;
}

sub Commit {
    my $self = shift;

    my $creatorObj = $self->TransactionObj->CreatorObj;
    
    my $programsParentName = 'EP Programs';
    
    my $programsParent = RT::Group->new(RT->SystemUser);
    $programsParent->LoadUserDefinedGroup($programsParentName);
    
    my $programs = $programsParent->GroupMembersObj;
    $programs->WithMember(PrincipalId => $creatorObj->id, Recursively => 0,);
    
    while (my $program = $programs->Next) {
	$self->TicketObj->AddWatcher(
	    PrincipalId => $program->id,
	    Type => 'AdminCc',
	    );
    }

    return 1;
}

1;
