package RT::Action::OVL_UpdateRegionAdminCcs;

use strict;
use warnings;
use base qw(RT::Action);

=head1 NAME

RT::Action::UpdateRegionAdminCcs - OVL's script to set regional access controls

=head1 DESCRIPTION

=cut

sub Prepare {
    return 1;
}

sub Commit {
    my $self = shift;
    RT::Extension::OurVoteLive->UpdateAdminCcRegionGroups($self->TicketObj);
    return 1;
}

1;
