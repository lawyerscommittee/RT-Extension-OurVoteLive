package RT::Action::OVL_SquelchVolunteerCreatorCcs;

use strict;
use warnings;
use base qw(RT::Action);

=head1 NAME

RT::Action::OVL_SquelchVolunteerCreatorCcs

=head1 DESCRIPTION

=cut

sub Prepare {
    my $self = shift;
    my $sharedUsers = RT::Group->new(RT->SystemUser);
    $sharedUsers->LoadUserDefinedGroup('EP Shared Users');

    if ($sharedUsers->HasMember($self->TicketObj->Creator)){
	return 1;
    }
    return 0;
}

sub Commit {
    my $self = shift;

    my @emails;
    push @emails, $self->TicketObj->Cc->MemberEmailAddresses;

    for my $email (@emails) {
        RT::Logger->warning($email);
        $self->TicketObj->SquelchMailTo($email);
    }

    return 1;
}

1;
