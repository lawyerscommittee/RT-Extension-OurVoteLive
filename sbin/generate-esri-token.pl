#!/usr/bin/env perl

use strict;
use warnings;

use RT;

RT::LoadConfig();

# Connect to the database. set up logging
#RT::Init();

my $domain = RT->Config->Get('WebDomain');
my $esriClientId = RT->Config->Get('EsriClientId');
my $esriClientSecret = RT->Config->Get('EsriClientSecret');


