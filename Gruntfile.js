module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                // the files to concatenate
                src: ['static/app/**/*.js'],
                // the location of the resulting JS file
                dest: 'static/js/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'static/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        html2js: {
            options: {
                base: 'static',
                module: 'ovl.templates'
            },
            main: {
                src: ['static/app/**/*.html'],
                dest: 'static/app/templates.js'
            },
        },
        watch: {
            files: ['static/app/**/*.js', 'static/app/**/*.html'],
            tasks: ['html2js', 'concat', 'uglify', 'notify:watch']
        },
        notify: {
            watch: {
                options: {
                    message: 'ovl.js generated'
                }
            }
        },
        "git-describe": {
            options: {},
            dist: {}
        }
    });
    
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-html2js');
    
    // the default task can be run just by typing "grunt" on the command line
    grunt.registerTask('default', ['html2js', 'concat', 'uglify']);

};