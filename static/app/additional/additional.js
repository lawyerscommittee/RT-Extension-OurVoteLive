'use strict';

angular.module('ovl.additional', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/additional', {
        templateUrl: 'app/additional/additional.html',
        controller: 'AdditionalCtrl'
    });
}])

.controller('AdditionalCtrl', ['$scope', '$location', 'ovlTicket',
function($scope, $location, ovlTicket) {
    $scope.ticket = ovlTicket;

    $scope.next = function() {
        $location.path('/finalize');
    };
}]);