'use strict';

// Declare app level module which depends on views, and components
angular.module('ovl', [
    'ngRoute',
    'ovl.login',
    'ovl.findCreate',
    'ovl.voter',
    'ovl.pollingplace',
    'ovl.details',
    'ovl.issueType',
    'ovl.additional',
    'ovl.finalize',
    'ovl.templates'
])

// Router configuration
.config(['$locationProvider', '$routeProvider', '$httpProvider',
function($locationProvider, $routeProvider, $httpProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/login'});
}])

// Specific router details that prevent users from going to the wrong page at the wrong time
.run(['$rootScope', '$location', '$timeout', 'ovlUser', 'ovlTicket',
function ($rootScope, $location, $timeout, ovlUser, ovlTicket) {
    $rootScope.$on('$routeChangeStart', function () {
        if(!ovlUser.name && $location.path() !== '/login') {
            // If the user isn't logged in, make them go to the login view
            $location.path('/login');
            return;
        } else if(ovlUser.name && $location.path() === '/login') {
            // If the user is logged in, don't let them go to the login view
            $location.path('/findCreate');
            return;
        } else if(ovlUser.name && !ovlTicket.phoneNumber && $location.path() !== '/findCreate') {
            // If there's no active ticket, make them go to the find create view
            $location.path('/findCreate');
            return;
        } else if(ovlUser.name && ovlTicket.phoneNumber && !ovlTicket.dirty && $location.path() !== '/finalize') {
            // If the active ticket isn't dirty, make them go to the finalize view
            $location.path('/finalize');
            return;
        }

        if($location.path() === '/findCreate') {
            $rootScope.setAutoLogoutTimer();
        } else if($rootScope.autoLogout) {
            $rootScope.clearAutoLogoutTimer();
        }
    });

    $rootScope.setAutoLogoutTimer = function() {
        $rootScope.autoLogoutWarn = $timeout(function() {
            $rootScope.alert.autoLogout = true;
        }, 150000); // 150000 ms == 2 min, 30 sec
        $rootScope.autoLogout = $timeout(function() {
            $rootScope.alert.autoLogout = false;
            ovlUser.clearUser();
            $location.path('/login');
        }, 180000); // 180000 ms == 3 min
    };

    $rootScope.clearAutoLogoutTimer = function() {
        $timeout.cancel($rootScope.autoLogoutWarn);
        $timeout.cancel($rootScope.autoLogout);
        $rootScope.autoLogoutWarn = null;
        $rootScope.autoLogout = null;
        $rootScope.alert.autoLogout = false;
    };
}])

// Nav controller, which handles login, logout, and navigation
.controller('NavCtrl', ['$scope', '$rootScope', '$location', 'ovlUser', 'ovlTicket',
function($scope, $rootScope, $location, ovlUser, ovlTicket) {
    $scope.user = ovlUser;
    $scope.ticket = ovlTicket;
    $rootScope.alert = {
        message: '',
        show: false,
        type: 'alert-success'
    };
    $scope.alert = $rootScope.alert;
    $scope.userIsHere = function() {
        $rootScope.clearAutoLogoutTimer();
        if($location.path() === '/findCreate') {
            $rootScope.setAutoLogoutTimer();
        }
    };

    $rootScope.$on('$routeChangeStart', function () {
        $scope.page = $location.path().substring(1);
    });

    $scope.clearUser = function() {
        ovlUser.clearUser();
        $location.path('/login');
    }

    $scope.logout = function() {
        ovlUser.clearUser();
        window.location = '/NoAuth/Logout.html';
    }
}])

// Service that keeps track of the user who is logged in
.factory('ovlUser', [function() {
    var user = {
        setUser: setUser,
        clearUser: clearUser,
        username: window.RT.CurrentUser.Name,
        name: sessionStorage.getItem('name'),
        email: sessionStorage.getItem('email')
    };
    return user;

    function setUser(name, email) {
        user.name = name;
        user.email = email;

        sessionStorage.setItem('name', name);
        sessionStorage.setItem('email', email);
    }

    function clearUser() {
        user.name = null;
        user.email = null;
        
        sessionStorage.removeItem('name');
        sessionStorage.removeItem('email');
    }
}])

// Service that keeps track of the ticket that is being worked on
.factory('ovlTicket', ['ovlUser', 'ovlUtils', function(ovlUser, ovlUtils) {
    var ticket = {};
    startOver();
    return ticket;

    function fromRtFormat(rtTicket) {
        startOver();
        ticket.dirty = false;

        var multiOptionFields = {
            'heardThrough': ticket.heardThrough,
            'race': ticket.race,
            'votingIssueType': ticket.votingIssueType
        };
        for (var key in rtTicket) {
            if (key === 'earlyVotingSite') {
                ticket[key] = !!rtTicket[key];
            } else if (key in multiOptionFields) {
                loadMultiOption(rtTicket[key], multiOptionFields[key]);
            } else {
                ticket[key] = rtTicket[key];
            }
        }
    }

    function loadMultiOption(valueToLoad, options) {
        var selectedOptions = valueToLoad.split(',');
        for(var i=0; i<options.length; i++) {
            if(selectedOptions.indexOf(options[i].Name) !== -1) {
                options[i].Selected = true;
            }
        }
    }

    function getTimeWorked() {
        // In minutes
        var duration = moment.duration(moment().diff(ticket.startedMoment));
        var minutes = Math.ceil(duration.asMinutes());
        return minutes;
    }

    function getTimeStartedStr() {
        // Thu Oct 13 12:59:25 2016
        return ticket.startedMoment.format('ddd MMM DD kk:mm:ss YYYY');
    }

    function toRtFormat() {
        // Add the normal fields
        var ticketData = [
            ['id', 'ticket/new'],
            ['Queue', 'Election Protection'],
            ['Cc', ovlUser.name + ' <' + ovlUser.email + '>'],
            ['Subject', ovlUtils.subjectFromText(ticket.text)],
            ['Text', ovlUtils.textToHtml(ticket.text)],
            ['Status', ticket.status]
        ];

        // If the ticket is being updated, we only need the status from the normal fields
        if (ticket.id) {
            ticketData = [
                ['Status', ticket.status]
            ];
        } else {
            ticketData.push(['TimeWorked', getTimeWorked() + ' minutes']);
            ticketData.push(['Started', getTimeStartedStr()]);
        }

        // Add the custom fields
        for (var customField in OVL.CFconfig["Election Protection"].CF) {
            var fieldName = ovlUtils.camelize(customField)
            if(fieldName in ticket) {
                var fieldValue = ticket[fieldName];
                if (Array.isArray(fieldValue)) {
                    multiOptionToRtFormat(ticketData, customField, fieldValue);
                } else if (fieldName === 'earlyVotingSite') {
                    ticketData.push(['CF.{' + customField + '}', (fieldValue ? customField : '')])
                } else {
                    ticketData.push(['CF.{' + customField + '}', fieldValue])
                }
            }
        }

        var ticketDataStr = '';
        for(var i=0; i<ticketData.length; i++) {
            ticketDataStr += ticketData[i][0] + ': ' + ticketData[i][1] + '\n';
        }
        return ticketDataStr;
    }

    function multiOptionToRtFormat(ticketData, fieldName, options) {
        // Get the values that were selected
        var values = [];
        angular.forEach(options, function(option) {
            if(option.Selected) values.push(option.Name);
        });

        if (ticket.id) {
            // If the ticket is being updated, RT expects a single field with
            // comma separated values
            ticketData.push(['CF.{' + fieldName + '}', values.join(',')]);
        } else {
            // If the ticket is being created, RT expects each value in a different field
            angular.forEach(values, function(value) {
                ticketData.push(['CF.{' + fieldName + '}', value]);
            });
        }
    }

    function startOver() {
        for (var prop in ticket) { 
            if (ticket.hasOwnProperty(prop)) {
                delete ticket[prop];
            }
        }
        ticket.startOver = startOver;
        ticket.getTimeWorked = getTimeWorked;
        ticket.fromRtFormat = fromRtFormat;
        ticket.toRtFormat = toRtFormat;
        ticket.dirty = true;
        ticket.startedMoment = moment();
        ticket.callerOptions = OVL.CFconfig['Election Protection'].CF['Caller'].Values;
        ticket.race = OVL.CFconfig['Election Protection'].CF['Race'].Values;
        ticket.heardThrough = OVL.CFconfig['Election Protection'].CF['Heard through'].Values;
        ticket.votingIssueType = OVL.CFconfig['Election Protection'].CF['Voting issue type'].Values;

        clearSelected(ticket.race);
        clearSelected(ticket.heardThrough);
        clearSelected(ticket.votingIssueType);

        ticket.detailsPlaceHolder = 'Completely describe the voter’s issue and what you did to help them. Include any relevant personal information about the voter.';
        ticket.detailsHelpBlock = 'You can add to or change the details on every page';
    }

    function clearSelected(list) {
        angular.forEach(list, function(item) {
            if(item.Selected) item.Selected = false;
        });
    }
}])

// Service that makes calls to OVL server (geocoder is also shoehorned in here)
.factory('ovlApi', ['$rootScope', '$http', 'ovlUser', 'ovlUtils', function($rootScope, $http, ovlUser, ovlUtils) {
    var apiService = {
        getTicket: getTicket,
        saveTicket: saveTicket,
        postComment: postComment,
        searchTicketsByPhoneNumber: searchTicketsByPhoneNumber,
        geocode: geocode,
        getPrecinct: getPrecinct,
        getPollingPlace: getPollingPlace,
        alertError: alertError
    };
    return apiService;

    function alertError(message) {
        $rootScope.alert.message = message;
        $rootScope.alert.type = 'alert-danger';
        $rootScope.alert.show = true;
    }

    // Returns a random integer between min (included) and max (excluded)
    // Using Math.round() will give you a non-uniform distribution!
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getPollingPlace(address, success, failure, startingKeyIdx, currentKeyIdx) {
        var keys = [
            'AIzaSyDFLO-Er1EpA2XkkKk5wukPAWn0Hcttb_0', // Ben Schoenfeld - lawyers-committee-ovl-1
            'AIzaSyCDIziuim69hBe0h3HQ7h0gse58Ar0qwS8', // Ben Schoenfeld - lawyers-committee-ovl-2
            'AIzaSyBc8QKeHqi-GO2AgiGsaXvOVUAVQPBbuZ0', // Ben Schoenfeld - lawyers-committee-ovl-3
            'AIzaSyBGtT1A0iMyMWVm2zR3VEJX-YTmt6Gz77A', // Ben Schoenfeld - quick180
            'AIzaSyCNsyojm0VjBde5v9AVunWHm1qBF2lCiKE'  // Ben Schoenfeld - va-service-mapping
        ];

        // Iterate through each key until we get back to the start or we successfully geocode
        if (startingKeyIdx === undefined) {
            startingKeyIdx = getRandomInt(0,keys.length);
            currentKeyIdx = startingKeyIdx;
        } else if (startingKeyIdx === currentKeyIdx) {
            failure && failure();
            return;
        }

        var url = 'https://www.googleapis.com/civicinfo/v2/voterinfo?address='
        url += address;
        url += '&key=' + keys[currentKeyIdx];
        $http.get(url).then(function(response) {
            // Verify the response is in the expected format
            if(!response.data.normalizedInput ||
                !response.data.pollingLocations ||
                !response.data.pollingLocations.length) {
                failure && failure();
                return;
            }

            // Build the polling place address string
            var normalized = response.data.normalizedInput;
            var pollingPlace = response.data.pollingLocations[0].address;
            var pollingPlaceNotes = response.data.pollingLocations[0].notes;
            var pollingHours = response.data.pollingLocations[0].pollingHours;
            var pollingPlaceAddress = '';
            if (pollingPlace.line1)
                pollingPlaceAddress += pollingPlace.line1 + ', ';
            if (pollingPlace.line2)
                pollingPlaceAddress += pollingPlace.line2 + ', ';
            if (pollingPlace.city)
                pollingPlaceAddress += pollingPlace.city + ', ';
            if (pollingPlace.state)
                pollingPlaceAddress += pollingPlace.state + ', ';
            if (pollingPlace.zip)
                pollingPlaceAddress += pollingPlace.zip;

            // Reutrn the result
            success && success({
                address: normalized.line1,
                city: normalized.city,
                state: normalized.state,
                zip: normalized.zip,
                pollingPlaceName: pollingPlace.locationName,
                pollingPlaceAddress: pollingPlaceAddress,
                pollingPlaceNotes: pollingPlaceNotes,
                pollingHours: pollingHours
            });
        }, function(response) {
            // If the address couldn't be parsed then give up
            if (response.data && response.data.error) {
                if (response.data.error.message == 'Failed to parse address') {
                    failure && failure();
                    return;
                }
            }

            // Any other error, try again with a different key
            currentKeyIdx = (currentKeyIdx + 1) % keys.length;
            getPollingPlace(address, success, failure, startingKeyIdx, currentKeyIdx);
        });
    }

    function getPrecinct(address, success, failure) {
        var url = 'https://geocoding.geo.census.gov/geocoder/geographies/onelineaddress?';
        url += 'benchmark=Public_AR_Census2010&';
        url += 'vintage=Census2010_Census2010&';
        url += 'address=' + address;
        url += '&layers=62,90&format=jsonp&callback=JSON_CALLBACK&';
        url += 'key=93d4648f4bc0f6610997ffa42f00434c9fe3798a';
        $http.jsonp(url).then(function(response) {
            // Verify the response is in the expected format
            if (!response.data.result ||
                !response.data.result.addressMatches ||
                !response.data.result.addressMatches.length) {
                failure && failure();
                return;
            }

            var addressMatch = response.data.result.addressMatches[0];
            if (!addressMatch.addressComponents ||
                !addressMatch.geographies ||
                !addressMatch.geographies['Counties'] ||
                !addressMatch.geographies['Counties'].length ||
                !addressMatch.geographies['Voting Districts'] ||
                !addressMatch.geographies['Voting Districts'].length) {
                failure && failure();
                return;
            }

            success && success({
                address: addressMatch.matchedAddress,
                coordinates: addressMatch.coordinates,
                city: addressMatch.addressComponents.city,
                state: addressMatch.addressComponents.state,
                zip: addressMatch.addressComponents.zip,
                county: addressMatch.geographies['Counties'][0].NAME,
                district: addressMatch.geographies['Voting Districts'][0].NAME
            });
        }, function(response) {
            failure && failure();
        });
    }

    function geocode(address, success, failure) {
        var url = 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?singleLine=';
        url += address + '&forStorage=false&outFields=*&f=pjson';
        if (window.ESRIToken) {
            url += '&token=' + window.ESRIToken.access_token;
        }
        $http.get(url).then(function(response) {
            if (response.data.error && response.data.error.code) {
                var error = response.data.error;
                alertError('ESRI Geocoder failed: ' + error.message + ' ' + error.code);
                failure && failure();
            } else {
                success && success(response.data);
            }
        }, function(response) {
            alertError('ESRI Geocoder failed: ' + response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function transform(data) {
        return $.param({'content':data});
    }

    function getTicket(id, success, failure) {
        var url = '/REST/1.0/ticket/';
        url += id;
        url += '/show';
        $http.get(url).then(function(response){
            console.log(response);
            var responseParts = response.data.split('\n');
            if(responseParts[0].indexOf('200 Ok') === -1) {
                alertError(response.data);
                failure && failure();
                return;
            }
            var ticket = {};
            for(var i=1; i<responseParts.length; i++) {
                var lineParts = responseParts[i].split(': ');
                if(lineParts.length !== 2) continue;
                var fieldName = lineParts[0];
                if(fieldName.indexOf('CF.') === 0) {
                    fieldName = fieldName.slice(4, -1);
                }
                fieldName = ovlUtils.camelize(fieldName);
                ticket[fieldName] = lineParts[1];

                if (fieldName === 'id') {
                    ticket[fieldName] = ticket[fieldName].split('/')[1];
                }
            }

            getAttachments(ticket.id, function(response) {
                if (response.attachmentIds.length) {
                    ticket.attachments = [];
                    angular.forEach(response.attachmentIds, function(attachmentId) {
                        getAttachmentContent(ticket.id, attachmentId, function(attachment) {
                            ticket.attachments.push(attachment);
                        })
                    });
                }
                success && success(ticket);
            }, failure);
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function getAttachments(ticketId, success, failure) {
        var url = '/REST/1.0/ticket/';
        url += ticketId;
        url += '/attachments';
        $http.get(url).then(function(response){
            var responseParts = response.data.split('\n');
            if(responseParts[0].indexOf('200 Ok') === -1) {
                success && success({
                    ticketId: ticketId,
                    attachmentIds: []
                });
                return;
            }

            var start = response.data.indexOf('Attachments:') + 'Attachments:'.length;
            var lines = response.data.substring(start).replace(/\s/g, '').split(',');
            var attachmentIds = [];
            for (var i=0; i<lines.length; i++) {
                attachmentIds.push(lines[i].split(':')[0]);
            }
            success && success({
                ticketId: ticketId,
                attachmentIds: attachmentIds
            });
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function getAttachmentContent(ticketId, attachmentId, success, failure) {
        var url = '/REST/1.0/ticket/';
        url += ticketId;
        url += '/attachments/';
        url += attachmentId;
        $http.get(url).then(function(response){
            var responseParts = response.data.split('\n');
            var contentIndex = response.data.indexOf('\nContent: ');
            if(responseParts[0].indexOf('200 Ok') === -1 || contentIndex === -1) {
                alertError(response.data);
                failure && failure();
                return;
            }
            responseParts = response.data.substring(0, contentIndex).split('\n');
            var attachment = {
                ticketId: ticketId,
                attachmentId: attachmentId,
                text: response.data.substring(contentIndex + '\nContent: '.length).replace(/^\s+|\s+$/g, '')
            };

            angular.forEach(responseParts, function(part) {
                if (part.indexOf('Created: ') === 0) {
                    attachment.created = part.split('Created: ')[1]
                } else if (part.replace(/^\s+/, '').indexOf('From:') === 0) {
                    attachment.createdBy = part.split('From: ')[1]
                }
            });

            if (!attachment.createdBy && attachment.text.indexOf('From: ') === 0) {
                var lineBreakIdx = attachment.text.indexOf('\n');
                if (lineBreakIdx !== -1) {
                    attachment.createdBy = attachment.text.substring('From: '.length, lineBreakIdx);
                    attachment.text = attachment.text.substring(lineBreakIdx + 1).replace(/^\s+|\s+$/g, '');
                }
            }

            if (!attachment.createdBy) {
                attachment.createdBy = 'Laywers Committee';
            }

            success && success(attachment);
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function postComment(ticket, success, failure) {
        var url = '/REST/1.0/ticket/';
        url += ticket.id;
        url += '/comment';

        ticket.newComment = 'From: ' + ovlUser.name + ' <' + ovlUser.email + '>\n\n' + ticket.newComment
        var ticketData = [
            ['id', ticket.id],
            ['Action', 'correspond'],
            ['Cc', ovlUser.name + ' <' + ovlUser.email + '>'],
            ['TimeWorked', ticket.getTimeWorked() + ' minutes'],
            ['Text', ovlUtils.textToHtml(ticket.newComment)]
        ];

        var ticketDataStr = '';
        for(var i=0; i<ticketData.length; i++) {
            ticketDataStr += ticketData[i][0] + ': ' + ticketData[i][1] + '\n';
        }

        $http.post(url, ticketDataStr, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            transformRequest: transform
        })
        .then(function(response){
            // Now that the ticket is saved, reset the start moment so that if
            // something else is added, the additional time worked is right
            ticket.startedMoment = moment();

            success && success();
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function saveTicket(ticket, success, failure) {
        var url = '/REST/1.0/ticket/';
        if (ticket.id) {
            url += ticket.id;
            url += '/edit';
        } else {
            url += 'new';
        }
        var ticketDataStr = ticket.toRtFormat();

        $http.post(url, ticketDataStr, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            transformRequest: transform
        })
        .then(function(response){
            console.log(response);
            if (ticket.id) {
                // Handle update response
                if (response.data.indexOf('# Ticket ' + ticket.id + ' updated.') !== -1) {
                    success && success({
                        id: ticket.id
                    });
                } else {
                    alertError(response.data);
                    failure && failure();
                }
            } else {
                // Handle create response
                var responseParts = response.data.split(' ');
                if(responseParts[responseParts.length-1].indexOf('created.') === 0) {
                    // Now that the ticket is saved, reset the start moment so that if
                    // something else is added, the additional time worked is right
                    ticket.startedMoment = moment();

                    success && success({
                        id: responseParts[responseParts.length-2]
                    });
                } else {
                    alertError(response.data);
                    failure && failure();
                }
            }
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }

    function searchTicketsByPhoneNumber(phoneNumber, success, failure) {
        var searchUrl = "/REST/1.0/search/ticket";
        searchUrl += "?query='CF.{Phone number}'='";
        searchUrl += phoneNumber;
        searchUrl += "'&orderby=-Created";
        $http.get(searchUrl)
        .then(function(response){
            console.log(response);
            var responseParts = response.data.split('\n');
            if(responseParts[0].indexOf('200 Ok') === -1) {
                alertError(response.data);
                failure && failure();
                return;
            }
            var tickets = [];
            for(var i=1; i<responseParts.length; i++) {
                var lineParts = responseParts[i].split(': ');
                if(lineParts.length !== 2) continue;
                tickets.push({
                    id: lineParts[0],
                    subject: lineParts[1]
                });
            }
            success && success(tickets);
        }, function(response) {
            alertError(response.status + ' ' + response.statusText);
            failure && failure();
        });
    }
}])

.factory('ovlUtils', function() {
    var utils = {
        subjectFromText: subjectFromText,
        textToHtml: textToHtml,
        textFromHtml: textFromHtml,
        camelize: camelize
    };
    return utils;

    function subjectFromText(text) {
        if (!text) return '';

	    // Exclusive. Max 200 characters in summary display.
	    var truncStr = text.substring(0,200);
	    var brIndex = truncStr.lastIndexOf("\n");
	    if (brIndex < 1) {
            brIndex = truncStr.lastIndexOf(".");
            brIndex++; // include '.' May drop off summary display.
	    }
	    if (brIndex < 1) {
		    brIndex = truncStr.lastIndexOf(" ");
	    }
	    if (brIndex < 1) {
		    return truncStr;
	    }
	    return truncStr.substring(0, brIndex);
	}

    function textToHtml(text) {
        if (!text) return '';
	    return text.replace(/\n/g, "\n ");
	}

    function textFromHtml(text) {
        if (!text) return '';
	    return text.replace(/\n/g, "<br/>");
	}

    function camelize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
            return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
        }).replace(/\s+/g, '');
    }
})

.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '').replace(/[-\s\(\)]/g, '');

        if (value.match(/[^0-9]/)) {
            return null; //return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                if(country !== '1') return null;
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                return null;
                //break;

            default:
                return null; //return tel;
        }

        if (country == 1) {
            country = "";
        }

        return '(' + city + ') ' + number.slice(0, 3) + '-' + number.slice(3);
        //number = number.slice(0, 3) + '-' + number.slice(3);
        //return (country + " (" + city + ") " + number).trim();
    };
});
