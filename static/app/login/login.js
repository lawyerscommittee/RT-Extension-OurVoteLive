'use strict';

angular.module('ovl.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
    });
}])

.controller('LoginCtrl', ['$scope', '$location', 'ovlUser', function($scope, $location, ovlUser) {
    $scope.submit = function() {
        if (!$scope.userName || !$scope.userEmail) return;
        ovlUser.setUser($scope.userName, $scope.userEmail);
        $location.path('/findCreate');
    };
}]);