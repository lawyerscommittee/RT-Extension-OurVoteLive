'use strict';

angular.module('ovl.pollingplace', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/pollingplace', {
        templateUrl: 'app/pollingplace/pollingplace.html',
        controller: 'PollingPlaceCtrl'
    });
}])

.controller('PollingPlaceCtrl', ['$scope', '$location', 'ovlApi', 'ovlTicket',
function($scope, $location, ovlApi, ovlTicket) {
    $scope.ticket = ovlTicket;

    $scope.lookupAddress = function() {
        $scope.ticket.pollingPlaceAddress = null;
        $scope.ticket.pollingPlaceLatitude = null;
        $scope.ticket.pollingPlaceLongitude = null;
        $scope.geocoding = true;

        ovlApi.geocode($scope.address, function(result) {
            if(result.candidates.length > 0) {
                $scope.ticket.pollingPlaceAddress = result.candidates[0].address;
                $scope.ticket.pollingPlaceLatitude = result.candidates[0].location.y;
                $scope.ticket.pollingPlaceLongitude = result.candidates[0].location.x;
            } else {
                $scope.ticket.pollingPlaceAddress = $scope.address;
            }
            $scope.geocoding = false;
        }, function() {
            $scope.ticket.pollingPlaceAddress = $scope.address;
            $scope.geocoding = false;
        });
    };

    $scope.lookupPrecinct = function() {
        if (!$scope.ticket.address) return;

        $scope.ticket.precinct = null;
        $scope.gettingPrecinct = true;

        var address = $scope.ticket.address;
        address += ', ' + $scope.ticket.zip;

        ovlApi.getPrecinct(address, function(response) {
            if (!response.district) {
                ovlApi.alertError('Sorry. Something went wrong looking up the precinct.');
            } else {
                $scope.ticket.precinct = response.district;
            }
            $scope.gettingPrecinct = false;
        }, function() {
            $scope.gettingPrecinct = false;
            ovlApi.alertError('Sorry. Something went wrong looking up the precinct.');
        });
    };

    $scope.lookupPollingPlace = function() {
        if (!$scope.ticket.address) return;

        $scope.ticket.pollingPlaceName = null;
        $scope.ticket.pollingPlaceAddress = null;
        $scope.address = null;
        $scope.pollingHours = null;
        $scope.pollingPlaceNotes = null;
        $scope.gettingPollingPlace = true;

        var address = $scope.ticket.address;
        address += ', ' + $scope.ticket.zip;

        ovlApi.getPollingPlace(address, function(response) {
            $scope.ticket.pollingPlaceName = response.pollingPlaceName;
            $scope.ticket.pollingPlaceAddress = response.pollingPlaceAddress;
            $scope.address = response.pollingPlaceAddress;
            $scope.pollingHours = response.pollingHours;
            $scope.pollingPlaceNotes = response.pollingPlaceNotes;
            $scope.gettingPollingPlace = false;
        }, function() {
            $scope.gettingPollingPlace = false;
            ovlApi.alertError('Sorry. Something went wrong looking up the polling place.');
        });
    };

    $scope.next = function() {
        $location.path('/details');
    };
}]);