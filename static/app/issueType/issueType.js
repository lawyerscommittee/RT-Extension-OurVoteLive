'use strict';

angular.module('ovl.issueType', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/issueType', {
        templateUrl: 'app/issueType/issueType.html',
        controller: 'IssueTypeCtrl'
    });
}])

.controller('IssueTypeCtrl', ['$scope', '$location', 'ovlTicket',
function($scope, $location, ovlTicket) {
    $scope.ticket = ovlTicket;
    $scope.next = function() {
        $location.path('/additional');
    };
}]);