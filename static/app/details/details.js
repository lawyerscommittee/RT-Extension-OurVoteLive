'use strict';

angular.module('ovl.details', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/details', {
        templateUrl: 'app/details/details.html',
        controller: 'DetailsCtrl'
    });
}])

.controller('DetailsCtrl', ['$scope', '$location', 'ovlTicket',
function($scope, $location, ovlTicket) {
    $scope.ticket = ovlTicket;

    $scope.next = function() {
        $location.path('/issueType');
    };
}]);