'use strict';

angular.module('ovl.voter', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/voter', {
        templateUrl: 'app/voter/voter.html',
        controller: 'VoterCtrl'
    });
}])

.controller('VoterCtrl', ['$scope', '$location', '$filter', 'ovlApi', 'ovlTicket',
function($scope, $location, $filter, ovlApi, ovlTicket) {
    $scope.ticket = ovlTicket;

    $scope.changePhoneNumber = function() {
        var phoneNumber = $filter('tel')($scope.ticket.phoneNumber);
        $scope.invalidPhoneNumber = !phoneNumber;
        if (phoneNumber) {
            $scope.ticket.phoneNumber = phoneNumber;
        }
    };

    $scope.lookupAddress = function() {
        $scope.ticket.address = null;
        $scope.ticket.city = null;
        $scope.ticket.county = null;
        $scope.ticket.state = null;
        $scope.ticket.zip = null;
        $scope.ticket.latitude = null;
        $scope.ticket.longitude = null;
        $scope.geocoding = true;

        ovlApi.geocode($scope.address, function(result) {
            if(result.candidates.length > 0) {
                $scope.ticket.address = result.candidates[0].attributes.StAddr;
                $scope.ticket.city = result.candidates[0].attributes.City || result.candidates[0].attributes.PlaceName;
                $scope.ticket.county = result.candidates[0].attributes.Subregion;
                $scope.ticket.state = result.candidates[0].attributes.Region;
                $scope.ticket.zip = result.candidates[0].attributes.Postal;
                $scope.ticket.latitude = result.candidates[0].location.y;
                $scope.ticket.longitude = result.candidates[0].location.x;
            } else {
                $scope.ticket.address = $scope.address;
            }
            $scope.geocoding = false;
        }, function() {
            $scope.ticket.address = $scope.address;
            $scope.geocoding = false;
        });
    };

    function lookupPollingPlace(callback) {
        var address = $scope.ticket.address;
        address += ', ' + $scope.ticket.zip;

        ovlApi.getPollingPlace(address, function(response) {
            $scope.ticket.pollingPlaceName = response.pollingPlaceName;
            $scope.ticket.pollingPlaceAddress = response.pollingPlaceAddress;
            $scope.address = response.pollingPlaceAddress;
            $scope.pollingHours = response.pollingHours;
            $scope.pollingPlaceNotes = response.pollingPlaceNotes;
            callback();
        }, function() {
            ovlApi.alertError('Sorry. Something went wrong looking up the polling place.');
            callback();
        });
    }

    function createTicket() {
        $scope.creatingTicket = true;
        ovlApi.saveTicket($scope.ticket, function(response) {
            $scope.ticket.id = response.id;
            $scope.ticket.dirty = false;
            $location.path('/finalize');
        }, function() {
            ovlApi.alertError('Sorry. Something went wrong creating ticket.');
            $scope.creatingTicket = false;
        });
    }

    $scope.createPollingPlaceTicket = function () {
        // Set default text
        if (!$scope.ticket.text) {
            $scope.ticket.text = '';
        }
        $scope.ticket.text = 'Polling Place Lookup\n' + $scope.ticket.text;

        // Set issue type
        $.each($scope.ticket.votingIssueType, function() {
            if (this.Description === 'Polling place') {
                this.Selected = true;
            }
        });

        // Set resolved
        $scope.ticket.status = 'resolved';

        // Lookup polling place
        if($scope.ticket.address) {
            lookupPollingPlace(createTicket);
        } else {
            createTicket();
        }
    };

    $scope.createRegistrationTicket = function () {
        // Set default text
        if (!$scope.ticket.text) {
            $scope.ticket.text = '';
        }
        $scope.ticket.text = 'Registration Lookup\n' + $scope.ticket.text;

        // Set issue type
        $.each($scope.ticket.votingIssueType, function() {
            if (this.Description === 'Registration') {
                this.Selected = true;
            }
        });

        // Set resolved
        $scope.ticket.status = 'resolved';
        createTicket();
    };

    $scope.next = function() {
        $location.path('/pollingplace');
    };
}]);