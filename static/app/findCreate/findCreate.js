'use strict';

angular.module('ovl.findCreate', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/findCreate', {
        templateUrl: 'app/findCreate/findCreate.html',
        controller: 'FindCreateCtrl'
    });
}])

.controller('FindCreateCtrl', ['$rootScope', '$scope', '$location', '$filter', 'ovlApi', 'ovlTicket',
function($rootScope, $scope, $location, $filter, ovlApi, ovlTicket) {

    $scope.createTicketWithoutPhoneNumber = function() {
        $rootScope.ticketsForPhoneNumber = null;
        ovlTicket.startOver();
        ovlTicket.phoneNumber = 'Not Available';
        $location.path('/voter');
    };

    $scope.findCreateTicket = function() {
        $rootScope.ticketsForPhoneNumber = null;
        ovlTicket.startOver();

        var phoneNumber = $filter('tel')($scope.phoneNumber);
        $scope.invalidPhoneNumber = !phoneNumber;
        if (!phoneNumber) return;

        $scope.phoneNumber = phoneNumber;

        // search tickets
        ovlApi.searchTicketsByPhoneNumber(phoneNumber, function(tickets) {
            if (tickets.length) {
                $rootScope.ticketsForPhoneNumber = tickets;
                ovlApi.getTicket(tickets[0].id, function(ticket) {
                    ovlTicket.fromRtFormat(ticket);
                    $location.path('/finalize');
                }, function() {
                    ovlTicket.phoneNumber = phoneNumber;
                    $location.path('/voter');
                });
            } else {
                ovlTicket.phoneNumber = phoneNumber;
                $location.path('/voter');
            }
        }, function() {
            ovlTicket.phoneNumber = phoneNumber;
            $location.path('/voter');
        });
    };
}]);