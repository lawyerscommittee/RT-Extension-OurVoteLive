'use strict';

angular.module('ovl.finalize', ['ngRoute', 'ngSanitize'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/finalize', {
        templateUrl: 'app/finalize/finalize.html',
        controller: 'FinalizeCtrl'
    });
}])

.controller('FinalizeCtrl', ['$rootScope', '$scope', '$filter', '$location', 'ovlApi', 'ovlTicket',
function($rootScope, $scope, $filter, $location, ovlApi, ovlTicket) {
    $scope.ticket = ovlTicket;
    $scope.ticketsForPhoneNumber = $rootScope.ticketsForPhoneNumber;
    if ($scope.ticketsForPhoneNumber) {
        $.each($scope.ticketsForPhoneNumber, function() {
            if (this.id === $scope.ticket.id) {
                $scope.selectedTicket = this;
            }
        });
    }

    $scope.ticketLabel = function(ticket) {
        return ticket.id + ': ' + ticket.subject;
    };

    $scope.switchTicket = function() {
        $scope.loadingTicket = true;
        ovlApi.getTicket($scope.selectedTicket.id, function(ticket) {
            ovlTicket.fromRtFormat(ticket);
            $scope.loadingTicket = false;
        }, function() {
            $scope.loadingTicket = false;
        });
    };

    function validateTicket() {
        var selectedVotingIssueType = $filter('filter')($scope.ticket.votingIssueType, {Selected: true});
        if (selectedVotingIssueType.length === 0) {
            $rootScope.alert.message = 'You must select at least one Voting Issue Type';
            $rootScope.alert.type = 'alert-danger';
            $rootScope.alert.show = true;
            return false;
        }
        return true;
    }

    $scope.escalate = function() {
        if (!validateTicket()) return;
        $scope.ticket.status = 'open';
        saveTicket();
    };

    $scope.resolve = function() {
        if (!validateTicket()) return;
        $scope.ticket.status = 'resolved';
        saveTicket();
    };

    function saveTicket() {
        $scope.saving = true;

        if ($scope.ticket.id && !$scope.ticket.newComment) {
            $scope.ticket.newComment = 'TICKET UPDATED BY VOLUNTEER';
        }

        ovlApi.saveTicket($scope.ticket, function(response) {
            $scope.ticket.id = response.id;
            $scope.ticket.dirty = false;
            $scope.saving = false;

            if ($scope.ticket.newComment) {
                $scope.addComment();
            }
        }, function() {
            $scope.saving = false;
        });
    }

    $scope.reopen = function() {
        $scope.ticket.dirty = true;
    };

    $scope.startOver = function() {
        $scope.ticket.startOver();
        $location.path('/findCreate');
    };

    $scope.newTicketWithThisNumber = function() {
        var phoneNumber = ovlTicket.phoneNumber;
        ovlTicket.startOver();
        ovlTicket.phoneNumber = phoneNumber;
        $location.path('/voter');
    };

    $scope.anyCategories = function() {
        for(var i=0; i<$scope.ticket.categoryTypes.length; i++) {
            if($scope.ticket.categoryTypes[i].activeCount) {
                return true;
            }
        }
        return false;
    };

    $scope.addComment = function() {
        ovlApi.postComment($scope.ticket, function() {
            $scope.ticket.newComment = null;
            ovlApi.getTicket($scope.ticket.id, function(ticket) {
                ovlTicket.fromRtFormat(ticket);
            });
        }, function() {
            // failure
        });
    };
}]);