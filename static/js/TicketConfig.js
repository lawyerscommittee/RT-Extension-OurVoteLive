window.OVL = window.OVL || {};
window.OVL.CFconfig = window.OVL.CFconfig || {};

window.OVL.CFconfig['Election Protection'] = (function () {

    var CF = {
		'Address:': {},
		'Caller': {
			'Values': [
				{'Description':'Voter','Name':'Voter'},
				{'Description':'Person assisting voter','Name':'Person assisting voter'},
				{'Description':'Election Protection volunteer','Name':'Election Protection volunteer'},
				{'Description':'Poll worker','Name':'Poll worker'},
				{'Description':'Other','Name':'Other '}
			]
		},
		'City': {},
		'County': {},
		'Early voting site': {},
		'First name': {},
		'Heard through': {
			'Values': [
				{'Description':'Fliers','Name':'Fliers'},
				{'Description':'Internet or social media','Name':'Internet or social media'},
				{'Description':'Newspaper','Name':'Newspaper'},
				{'Description':'Person at polling place','Name':'Person at polling place'},
				{'Description':'Radio','Name':'Radio'},
				{'Description':'TV','Name':'TV'},
				{'Description':'Word of mouth','Name':'Word of mouth'}
			]
		},
		'Last name': {},
		'Latitude': {},
		'Longitude': {},
		'Phone number': {},
		'Polling place address': {},
		'Polling place latitude': {},
		'Polling place longitude': {},
		'Polling place name': {},
		'Precinct': {},
		'Race': {
			'Values': [
				{'Description':'Asian','Name':'Asian'},
				{'Description':'American Indian or Alaska Native','Name':'American Indian or Alaska Native'},
				{'Description':'Black or African American','Name':'Black or African American'},
				{'Description':'Hispanic or Latino','Name':'Hispanic or Latino'},
				{'Description':'Middle Eastern or North African','Name':'Middle Eastern or North African'},
				{'Description':'Native Hawaiian or Pacific Islander','Name':'Native Hawaiian or Pacific Islander'},
				{'Description':'White','Name':'White'}
			]
		},
		'State': {},
		'Voting issue type': {
			'Values': [
				{'Description':'Polling place','Name':'Polling place'},
				{'Description':'Registration','Name':'Registration'},
				{'Description':'Voter ID','Name':'Voter ID'},
				{'Description':'Arrest or conviction','Name':'Arrest or conviction'},
				{'Description':'Ballots','Name':'Ballots'},
				{'Description':'Equipment','Name':'Equipment'},
				{'Description':'Accessibility','Name':'Accessibility'},
				{'Description':'Intimidation or challenges','Name':'Intimidation or challenges'},
				{'Description':'General information','Name':'General information'}
			]
		},
		'Zip': {},
	};
    return {
        CF: CF
    };
}());

